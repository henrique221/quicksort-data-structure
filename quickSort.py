import random
def geraListaDeNumerosAleatorios(tamanho):
    lista = []
    for i in range (tamanho):
        aleatorio = random.randint(1, 50)
        lista.append(aleatorio)
    return lista

def troca(lista, ini, fim):
    aux = lista[ini]
    lista[ini] = lista[fim]
    lista[fim] = aux

def ordenaListaPorQuickSort(lista):
    pivot = lista[0]
    ini = 1
    fim = len(lista)-1
    while ini<=fim:
        if lista[ini]<pivot:
            ini = ini+1
        elif lista[fim]>pivot:
            fim = fim-1
        else:
            troca(lista, ini, fim)
            ini = ini+1
            fim = fim-1
        troca(lista, ini, fim)
    return lista

def particao(lista, ini, fim):
    pivot=(lista[ini])
    ini = 1 
    i = ini+1
    j = fim
    fim = len(lista)-1
    while i <= j:
        if lista[i] <= pivot:
            i = i+1
        elif lista[j] > pivot:
            j = j-1
        else:    
            troca(lista, i, j)
            i=i+1
            j=j-1
        troca(lista, ini, j)
    return j

def quicksort(lista, ini, fim):
    if ini<fim:
        p=particao(lista, ini, fim)
        quicksort(lista, ini, p-1)
        quicksort(lista, p+1, fim)
    
    


if __name__ == '__main__':
    tamanho = int(input("Tamanho : "))
    lista = geraListaDeNumerosAleatorios(tamanho)
    print(lista)
    listaOrd = ordenaListaPorQuickSort(lista)
    print(listaOrd[3])
    print('ini =', listaOrd[0], '\nfim = ', listaOrd[1], '\npivot = ', listaOrd[2])
    print(quicksort(lista, 0, len(lista)-1))
    print(lista)
    
    

